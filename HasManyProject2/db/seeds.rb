# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Specialists = Specialist.create ([{name: 'John Bill'}, {specialty: 'Dentist'}])
Specialists = Specialist.create ([{name: 'Rob Neal'}, {specialty: 'Family Doctor'}])
Specialists = Specialist.create ([{name: 'Mark DelSor'}, {specialty: 'Cardiologists‎t'}])
Specialists = Specialist.create ([{name: 'Sara Lee'}, {specialty: 'Dermatologists'}])
Specialists = Specialist.create ([{name: 'Sam Harris'}, {specialty: 'Neurologists'}])
Insurances = Insurance.create ([{name: 'Blue Cross and Blue Shield of Texas'}, {street_address: '1001 E. Lookout Drive Richardson, Texas 75082'}])
Insurances = Insurance.create ([{name: 'UnitedHealthCare'}, {street_address: '1250 Capital of TX Hwy South, Bldg. 1 Ste 400 , Austin , TX 78746'}])
Insurances = Insurance.create ([{name: 'Selected Benefits'}, {street_address: '1800 W Loop S Fwy #600, Houston, TX 77027'}])
Insurances = Insurance.create ([{name: 'Texas Family Benefits'}, {street_address: '12946 S Dairy Ashford Rd, Sugar Land, TX 77478'}])
Insurances = Insurance.create ([{name: 'MHealth, Inc.'}, {street_address: '929 Gessner Rd., Ste 1500, Houston, TX 77024'}])
Patients = Patient.create ([{insurance_id: 5 }, {name: 'Miller Buck'}, {street_address: '1234 Carry Road Houston, TX 77654'}])
Patients = Patient.create ([{insurance_id: 6 }, {name: 'Jay Patel'}, {street_address: '9876 Glen Mission Houston, TX 77654'}])
Patients = Patient.create ([{insurance_id: 9 }, {name: 'Avi Polish'}, {street_address: '9876 Hiper Lane Houston, TX 77651'}])
Patients = Patient.create ([{insurance_id: 7 }, {name: 'Tony Irrera'}, {street_address: '8712 Area Rd. Houston, TX 77612'}])
Patients = Patient.create ([{insurance_id: 8 }, {name: ' 	Rich Vos'}, {street_address: '2354 Park Point Dr, Houston,TX 77999'}])
Appointments = Appointment.create ([{specialist_id: 2 }, {patient_id: 7}, {complaint: 'none'},{appointment_date: 2014-09-27 }])
Appointments = Appointment.create ([{specialist_id: 1 }, {patient_id: 8}, {complaint: 'none'},{appointment_date: 2014-10-27 }])
Appointments = Appointment.create ([{specialist_id: 3 }, {patient_id: 7}, {complaint: 'None'},{appointment_date: 2014-10-13 }])
Appointments = Appointment.create ([{specialist_id: 4 }, {patient_id: 8}, {complaint: 'Sick'},{appointment_date: 2014-10-18 }])
Appointments = Appointment.create ([{specialist_id: 5 }, {patient_id: 9}, {complaint: 'None'},{appointment_date: 2014-10-27 }])
Appointments = Appointment.create ([{specialist_id: 1 }, {patient_id: 9}, {complaint: 'None'},{appointment_date: 2014-11-16 }])
Appointments = Appointment.create ([{specialist_id: 3 }, {patient_id: 10}, {complaint: 'None'},{appointment_date: 2014-10-20 }])
Appointments = Appointment.create ([{specialist_id: 5 }, {patient_id: 10}, {complaint: 'None'},{appointment_date: 2014-10-11 }])
Appointments = Appointment.create ([{specialist_id: 2 }, {patient_id: 2}, {complaint: 'None'},{appointment_date: 2014-10-09 }])
Appointments = Appointment.create ([{specialist_id: 5 }, {patient_id: 11}, {complaint: 'None'},{appointment_date: 2014-10-05 }])
