class Patient < ActiveRecord::Base
  has_many :appointment
  has_many :specialist, :through => :appointment
  has_one :insurance

end
